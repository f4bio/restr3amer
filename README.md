# restr3amer

## ffmpeg command dummy audio

```bash
ffmpeg \
  -rtsp_transport tcp \
  -i $INPUT_STREAM_URL \
  -f lavfi -i anullsrc \
  -acodec copy \
  -vcodec copy \
  -map 0:v -map 1:a \
  -preset superfast \
  -f flv \
  $OUTPUT_STREAM_URL
```
