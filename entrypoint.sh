#!/bin/bash

# variant 1
ffmpeg -rtsp_transport tcp \
  -i $INPUT_STREAM_URL \
  -f lavfi -i anullsrc \
  -acodec copy \
  -vcodec copy \
  -map 1:a -map 0:v \
  -preset superfast \
  -f flv \
  $OUTPUT_STREAM_URL

# variant 2:
# ffmpeg -re \
#   -rtsp_transport tcp \
#   -i $INPUT_STREAM_URL \
#   -f lavfi -i anullsrc \
#   -c:v libx264 \
#   -preset veryfast \
#   -b:v 3000k \
#   -maxrate 3000k \
#   -bufsize 6000k \
#   -pix_fmt yuv420p \
#   -g 50 \
#   -c:a aac \
#   -b:a 160k \
#   -ac 2 \
#   -ar 44100 \
#   -f flv \
#   $OUTPUT_STREAM_URL
