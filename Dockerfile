FROM alpine:latest

RUN apk --quiet update
RUN apk --quiet add --no-cache bash ffmpeg

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

CMD ["/entrypoint.sh"]
